from setuptools import find_packages, setup

setup(
    name='src',
    packages=find_packages(),
    version='0.1.0',
    description='Study project of MlOps Course',
    author='Roman Breus',
    license='',
)
