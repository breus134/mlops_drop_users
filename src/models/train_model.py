import os
from dotenv import load_dotenv

from typing import List

import click
import pandas as pd
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import roc_auc_score
import joblib

from mlflow import log_params, log_metrics
from mlflow.models import infer_signature
import mlflow


load_dotenv()
remote_server_uri = os.getenv("MLFLOW_TRACKING_URI")
mlflow.set_tracking_uri(remote_server_uri)
mlflow.set_experiment("drop_users_rf")


@click.command()
@click.argument("input_path", type=click.Path(exists=True), nargs=4)
@click.argument("output_path", type=click.Path())
def train(input_path: List[str], output_path: str):
    """
    Train model and log params, metrics, artefacts
    @param input_path: path for train([0]) and test([1]) dataframe
    @param output_path:path for model([0]) and score([1])
    @return: None
    """
    params = {
        "n_estimators": 100,
        "n_jobs": 2,
        "min_samples_leaf": 10,
        "min_samples_split": 10,
        "class_weight": "balanced",
    }
    rf = RandomForestClassifier(**params)

    X_train = pd.read_csv(input_path[0])
    X_test = pd.read_csv(input_path[1])
    y_train = pd.read_csv(input_path[2])
    y_test = pd.read_csv(input_path[3])

    rf.fit(X_train, y_train)
    joblib.dump(rf, output_path)

    pred_proba = rf.predict_proba(X_test)
    score = dict(roc_score=roc_auc_score(y_test, pred_proba[:, 1]))

    signature = infer_signature(X_test, pred_proba)

    log_params(params)
    log_metrics(score)
    mlflow.sklearn.log_model(sk_model=rf,
                             artifact_path="model",
                             signature=signature,
                             registered_model_name="drop-users_rf")


if __name__ == "__main__":
    train()
