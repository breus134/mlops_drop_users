from typing import List

import pandas as pd
from sklearn.model_selection import train_test_split
import click


@click.command()
@click.argument("input_path", type=click.Path(exists=True), nargs=2)
@click.argument("output_path", type=click.Path(), nargs=4)
@click.argument("test_size", type=click.FLOAT)
def prepare_datasets(input_path: List[str],
                     output_path: List[str],
                     test_size: float = 0.1) -> None:
    """
    Split dataset to train and test datasets

    @param input_path: Path to read users feature and label DataFrame`s
    @param output_path: Path to save train and test DataFrame`s
    @param test_size: Size of test dataset
    @return: None
    """
    X_cv = pd.read_csv(input_path[0])
    y_cv = pd.read_csv(input_path[1])

    X_train, X_test, y_train, y_test = train_test_split(X_cv,
                                                        y_cv,
                                                        test_size=test_size,
                                                        stratify=y_cv)

    X_train.to_csv(output_path[0], index=False)
    X_test.to_csv(output_path[1], index=False)
    y_train.to_csv(output_path[2], index=False)
    y_test.to_csv(output_path[3], index=False)


if __name__ == '__main__':
    prepare_datasets()
