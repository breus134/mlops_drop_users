import pandas as pd
import click


@click.command()
@click.argument("input_path", type=click.Path(exists=True))
@click.argument("output_path", type=click.Path())
@click.argument("n_day", type=click.INT)
def truncate_data_nday(input_path: str, output_path: str, n_day: int) -> None:
    """Get data for n_day days for each user
    @param input_path: output_path: Path to save a truncated DataFrame
    @param output_path: input_path: Path to read a preprocessed DataFrame
    @param n_day: amount first days
    """
    data = pd.read_csv(input_path)

    users_min_time = (
        data.groupby("user_id", as_index=False)
        .agg({"timestamp": "min"})
        .rename({"timestamp": "min_timestamp"}, axis=1)
    )
    users_min_time["min_timestamp"] += 60 * 60 * 24 * n_day

    events_data_d = pd.merge(data, users_min_time, how="inner", on="user_id")
    cond = events_data_d["timestamp"] <= events_data_d["min_timestamp"]
    events_data_d = events_data_d[cond]
    events_data_d = events_data_d.drop(["min_timestamp"], axis=1)

    assert events_data_d.user_id.nunique() == data.user_id.nunique()
    events_data_d.to_csv(output_path, index=False)


if __name__ == "__main__":
    truncate_data_nday()
