import pandas as pd
import click


@click.command()
@click.argument("input_path", type=click.Path(exists=True))
@click.argument("output_path", type=click.Path())
def preprocess_timestamp_cols(input_path: str, output_path: str) -> None:
    """Create new columns: 'date' and 'day'
    :param input_path: Path to read a raw DataFrame
    :param output_path: Path to save a preprocessed DataFrame
    :return:
    """
    data = pd.read_csv(input_path)

    data["date"] = pd.to_datetime(data.timestamp, unit="s")
    data["day"] = data.date.dt.date

    data.to_csv(output_path, index=False)


if __name__ == "__main__":
    preprocess_timestamp_cols()
