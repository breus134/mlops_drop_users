import numpy as np
import pandas as pd
import click


def create_interaction(events: pd.DataFrame, submissions: pd.DataFrame) -> pd.DataFrame:
    """
    concat data
    @param events: DataFrame of events of users
    @param submissions: DataFrame of submissions of users
    @return conceited DataFrame:
    """
    interact_train = pd.concat(
        [events, submissions.rename(columns={"submission_status": "action"})]
    )
    interact_train.action = pd.Categorical(
        interact_train.action,
        ["discovered", "viewed", "started_attempt", "wrong", "passed", "correct"],
        ordered=True,
    )
    interact_train = interact_train.sort_values(["user_id", "timestamp", "action"])
    return interact_train


@click.command()
@click.argument("input_path_events", type=click.Path(exists=True))
@click.argument("input_path_submissions", type=click.Path(exists=True))
@click.argument("output_path", type=click.Path())
@click.argument("course_threshold", type=click.INT)
def create_user_label(
    input_path_events: str,
    input_path_submissions: str,
    output_path: str,
    course_threshold: int = 40,
):
    """
    Creation of label for each user
    @param input_path_events: Path to read a DataFrame of events of users
    @param input_path_submissions: Path to read a DataFrame of submissions of users
    @param output_path: Path to save created user label
    @param course_threshold: num of correct task for complete course
    @return None:
    """

    events = pd.read_csv(input_path_events)
    submissions = pd.read_csv(input_path_submissions)
    interactions = create_interaction(events, submissions)
    users_label = interactions[["user_id"]].drop_duplicates()

    assert "correct" in interactions.action.unique()

    passed_steps = (
        interactions.query("action == 'correct'")
        .groupby("user_id", as_index=False)["step_id"]
        .agg(lambda a: len(np.unique(a)))
        .rename(columns={"step_id": "correct"})
    )
    users_label = users_label.merge(passed_steps, how="outer")

    # пройден ли курс
    users_label["is_gone"] = users_label["correct"] > course_threshold
    assert users_label.user_id.nunique() == events.user_id.nunique()
    users_label = users_label.drop("correct", axis=1).set_index("user_id")
    users_label.to_csv(output_path, index=False)


if __name__ == "__main__":
    create_user_label()
