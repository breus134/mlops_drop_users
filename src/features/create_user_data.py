import pandas as pd
import click


@click.command()
@click.argument("input_path_events", type=click.Path(exists=True))
@click.argument("input_path_submissions", type=click.Path(exists=True))
@click.argument("output_path", type=click.Path())
def create_user_data(
    input_path_events: str, input_path_submissions: str, output_path: str
) -> None:
    """Creation of a table of features for each user
    @param input_path_events: Path to read a DataFrame of events of users
    @param input_path_submissions: Path to read a DataFrame of submissions of users
    @param output_path: Path to save created user data
    """
    events = pd.read_csv(input_path_events)
    submissions = pd.read_csv(input_path_submissions)

    users_data = (
        events.groupby("user_id", as_index=False)
        .agg({"timestamp": "max"})
        .rename(columns={"timestamp": "last_timestamp"})
    )

    users_scores = submissions.pivot_table(
        index="user_id",
        columns="submission_status",
        values="step_id",
        aggfunc="count",
        fill_value=0,
    ).reset_index()
    users_data = users_data.merge(users_scores, on="user_id", how="outer")
    users_data = users_data.fillna(0)

    users_events_data = events.pivot_table(
        index="user_id",
        columns="action",
        values="step_id",
        aggfunc="count",
        fill_value=0,
    ).reset_index()
    users_data = users_data.merge(users_events_data, how="outer")

    users_days = events.groupby("user_id").day.nunique().to_frame().reset_index()
    users_data = users_data.merge(users_days, how="outer")
    users_data.set_index("user_id")
    users_data.drop("last_timestamp", axis=1, inplace=True)

    users_data.to_csv(output_path, index=False)


if __name__ == "__main__":
    create_user_data()
